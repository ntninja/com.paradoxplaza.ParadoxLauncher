#!/bin/sh
set -e

APPID="com.paradoxplaza.ParadoxLauncher"

PATH_BUILD="build"
PATH_REPO="repo"

flatpak-builder --ccache --force-clean --repo "${PATH_REPO}" "$@" "${PATH_BUILD}" "${APPID}.json"